# TCP Service Mock #

While developing a binding for OpenHab to be able to remotely control my Marantz receiver, I missed a utility to be able to remotely develop this component while I was mostly working on this while I was commuting to and from work.
Strangely, I could nowhere find anything to be able to mock a tcp service, something that would listen on a port and return pre-defined responses.

So, that is what this utility does. It starts a service on a port which returns pre-defined responses based on the input. Under the hood, the utility uses Apache Camel to define a 'route', based on Netty, to process incoming data and return responses.

The service supports 2 different modes: expectations and scripts.

### Modes ###
* Expectations : the service always returns the same response to an input. Only one response possible to a specific input. For example: 'hello' will always return 'world'.
* Script : the service returns responses bases on a pre-defined script. The requests must arrive in the pre-described order. This allows multiple different responses to the same input. For example: First time 'hello' returns 'world', the seconds time 'hello' returns 'again'.

### External ###
The mock supports expectations defined in code and in external property files.

In code:
```
#!java
  mockTcpSerice.when("echo").thenAnswer("cho ho o");
```

In properties file: contains multiple lines. Each line in format:
```
#!xml
<input>=<output>
```

## How to use this? ##

* Clone repository
* Build and deploy in (local) repository
* Import Maven dependency in your project

```
#!xml
  <dependency>
    <groupId>eu.diversit.tcp-service</groupId>
    <artifactId>tcp-service-mock</artifactId>
    <version>1.0-SNAPSHOT</version>
  </dependency>
```

* See the [MockTcpSericeTest](https://bitbucket.org/diversit/tcp-service-mock/src/eeea6b447ff5e86e2160d9767ddcc4e893827365/src/test/java/eu/diversit/tcpservice/MockTcpSericeTest.java?at=master) how to use the mock in different modes and defined in code and in property files.