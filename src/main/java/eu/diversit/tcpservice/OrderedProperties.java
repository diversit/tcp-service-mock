package eu.diversit.tcpservice;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * A {@link java.util.Properties} / {@link java.util.Map} like class
 * which keeps the properties in the order they are loaded from the source file
 * and allows multiple similar keys.
 */
final public class OrderedProperties {

    private static final Function<Entry,String> ENTRY_TO_KEY_FUNCTION = new Function<Entry, String>() {
        @Override
        public String apply(Entry entry) {
            return entry.key;
        }
    };

    final private ImmutableList<Entry> properties;

    private OrderedProperties(final List<Entry> properties) {
        this.properties = ImmutableList.copyOf(properties);
    }

    private OrderedProperties(final ImmutableList<Entry> properties) {
        this.properties = properties;
    }

    public static OrderedProperties empty() {
        return new OrderedProperties(new ArrayList<Entry>());
    }

    public static OrderedProperties from(final Properties properties) {

        List<Entry> propList = new ArrayList<>(properties.size());
        for(String key : properties.stringPropertyNames()) {
            propList.add(new Entry(key, properties.getProperty(key)));
        }

        return new OrderedProperties(propList);
    }

    public static OrderedProperties loadFrom(final InputStream inputStream) throws IOException {

        final List<Entry> keyValues = new ArrayList<>();
        final BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        while(reader.ready()) {
            final String line = reader.readLine();

            if(line.startsWith("#") || isBlank(line)) {
                // skip comments and empty lines
            } else {
                String[] keyAndValue = splitOnFirstEquals(line);
                keyValues.add(new Entry(keyAndValue[0], keyAndValue[1]));
            }
        }

        return new OrderedProperties(keyValues);
    }

    private static boolean isBlank(final String line) {
        return line == null || "".equals(line.trim());
    }

    final private static String[] splitOnFirstEquals(final String line) {
        final int equalsIndex = line.indexOf("=");
        if(equalsIndex <= 0) {
            throw new IllegalArgumentException("Invalid line: "+line);
        }

        String key = line.substring(0, equalsIndex);
        String value = line.substring(equalsIndex+1);

        return new String[] { key, value };
    }

    final public boolean isEmpty() {
        return properties.isEmpty();
    }

    final public boolean isNotEmpty() {
        return !isEmpty();
    }

    final public int size() {
        return properties.size();
    }

    /**
     * @return Option containing first element first element. Is None if no elements left.
     */
    final public Option<Entry> head() {
        if(isEmpty()) {
            return Option.none();
        } else {
            return Option.of(properties.get(0));
        }
    }

    final public OrderedProperties tail() {
        if(isEmpty()) {
            throw new IllegalStateException("No elements left");
        } else {
            return new OrderedProperties(properties.subList(1, size()));
        }
    }

    final public List<String> keysList() {

        final Iterable<String> keys = Iterables.transform(properties, ENTRY_TO_KEY_FUNCTION);
        return ImmutableList.copyOf(keys);
    }

    final public OrderedProperties addEntry(final String key, final String value) {
        final ImmutableList<Entry> newProperties = new ImmutableList.Builder<Entry>()
                .addAll(properties)
                .add(new Entry(key, value))
                .build();

        return new OrderedProperties(newProperties);
    }

    final public boolean containsKey(final String key) {
        if(isEmpty()) {
            return false;
        }

        for(Entry entry : properties) {
            if(entry.getKey().equals(key)) {
                return true;
            }
        }

        return false;
    }

    final public String get(final String key) {
        if(isEmpty()) {
            return null;
        }

        for(Entry entry : properties) {
            if(entry.getKey().equals(key)) {
                return entry.getValue();
            }
        }

        return null;
    }

    /**
     * An entry in the ordered list
     */
    static class Entry {
        private final String key;
        private final String value;

        Entry(String key, String value){
            this.key = key;
            this.value = value;
        }

        final public String getKey() {
            return key;
        }

        final public String getValue() {
            return value;
        }

        @Override
        final public String toString() {
            return String.format("Entry[key=%s, value=%s]", key, value);
        }
    }
}
