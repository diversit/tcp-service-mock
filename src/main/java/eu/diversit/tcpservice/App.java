package eu.diversit.tcpservice;

import org.apache.camel.*;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.impl.DefaultCamelContext;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.concurrent.Future;

public class App {

    private final ProducerTemplate producerTemplate;
    private final Endpoint clientEndpoint;
    private final MockTcpService mockTcpService;

    public static void main(String[] args) throws Exception {
        String host = args[0];
        String cmd = args[1];
        System.out.println(String.format("Sending command '%s' to host '%s'", cmd, host));

        App app = new App(host, 8023);
        app.send(cmd);
    }

    public App(final String host, final int port) throws Exception {

        mockTcpService = new MockTcpService(port);
        mockTcpService.start();

        final CamelContext camelContext = new DefaultCamelContext();
        registerShutdownHook(camelContext);

        camelContext.addRoutes(new RouteBuilder() {
            @Override
            public void configure() throws Exception {
                from("direct:client")
                        .to("log:camel.route?showAll=true&multiline=true")
//                        .toF("netty:tcp://%s:%d?keepAlive=false&connectTimeout=200&sync=true&requestTimeout=1000&disconnect=true&noReplyLogLevel=ERROR&textline=true&autoAppendDelimiter=false", host, port)
                        .process(new SimpleTcpProcessor(host, port, "\r\n"))
                ;
            }
        });
        camelContext.start();

        clientEndpoint = camelContext.getEndpoint("direct:client");
        producerTemplate = camelContext.createProducerTemplate();
        producerTemplate.setDefaultEndpoint(clientEndpoint);
    }

    private void send(String cmd) throws Exception {

//        Object response = producerTemplate.requestBody(cmd);
        Future<Object> futureResponse = producerTemplate.asyncRequestBody(clientEndpoint, cmd);
        String response = (String) futureResponse.get();
        System.out.println("Response: "+response);
        System.exit(0);
    }

    private void registerShutdownHook(final CamelContext camelContext) {
        Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
            public void run() {
                try {
                    System.out.println("Stopping context...");
                    camelContext.stop();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    mockTcpService.stop();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }));
    }

    class SimpleTcpProcessor implements Processor {
        private final String host;
        private final int port;
        private String delimiter;

        public SimpleTcpProcessor(String host, int port, String delimiter) {
            this.host = host;
            this.port = port;
            this.delimiter = delimiter;
        }
        public void process(Exchange exchange) throws Exception {
            String cmd = (String) exchange.getIn().getBody();

            Socket socket = new Socket(host, port);
            DataOutputStream das = new DataOutputStream(socket.getOutputStream());
            BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));

            das.writeBytes(cmd + delimiter);

            StringBuilder response = new StringBuilder();
            while(response.length() == 0 || reader.ready()) {
                char c = (char) reader.read();
                response.append(c);
            }

            exchange.getOut().setBody(response.toString());

            socket.close();
        }
    }
}
