package eu.diversit.tcpservice;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.impl.DefaultCamelContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;

/**
 * A service which mocks a tcp service.
 * Responses to commands can be preloaded. Two modes are supported: any or script.
 * - In Any mode, for any response received which is in the list the corresponding response is returned.
 * - In Script mode, commands are processed one after the other and command are expected in certain order.
 *   Once a response to a command has been send, it is removed from the list and the next command is expected.
 *   In this mode you can test with a predefined script how the device should respond.
 *
 * The service implements {@link java.lang.AutoCloseable} so it can easily be used in the new
 * try-with-resources expression.
 * The service always runs on 'localhost' (of course). The port can be configured.
 */
final public class MockTcpService implements AutoCloseable {

    private static final Logger log = LoggerFactory.getLogger(MockTcpService.class);

    public static final Reply REPLY_INPUT = new Reply() {
        @Override
        public String reply(String input) {
            if("close".equals(input)) {
                log.info("Stopping service.");
                System.exit(1);
            }
            return input;
        }
    };

    public static final Reply REPLY_INPUT_UNKNOWN = new Reply() {
        @Override
        public String reply(String input) {
            return String.format("Unexpected command '%s'", input);
        }
    };

    final private int port;
    private DefaultCamelContext camelContext;
    private Reply defaultReply = REPLY_INPUT;
    private Expectations expectations = new AnyExpectations();

    public static void main(final String[] args) throws Exception {
        // get arguments
        final Option<String> filename = getArgument(args, 0);
        final String mode = getArgument(args, 1).getOrElse("any");

        // validate arguments
        if(filename.hasNoValue()) {
            printUsage("Filename missing");
        }

        if(!"any".equals(mode) && !"script".equals(mode)) {
            printUsage("Invalid mode");
        }

        // create service
        final MockTcpService mockTcpService = new MockTcpService(8023);

        // load file
        if("any".equals(mode)) {
            mockTcpService.loadExpectations(filename.get());
        } else {
            mockTcpService.loadScript(filename.get());
        }

        // start service
        mockTcpService.start();

        // register close on shutdown
        Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    mockTcpService.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }));

        // keep running until stopped
        while(true) { Thread.sleep(1000L); }
    }

    private static void printUsage(final String errorMsg) {
        log.error("Error: {}", errorMsg);
        log.error("Usage: MockTcpService <filename> [mode]");
        log.error("Mode must be either 'any' (default) or 'script");
        System.exit(1);
    }

    private static Option<String> getArgument(final String[] args, int argNr) {
        return args.length >= argNr ? Option.of(args[argNr]) : Option.none();
    }

    public MockTcpService(final int port) {

        this.port = port;
    }

    final public void start() throws Exception {
        camelContext = new DefaultCamelContext();

        camelContext.addRoutes(new RouteBuilder() {
            @Override
            public void configure() throws Exception {
                fromF("netty:tcp://localhost:%d?sync=true&textline=true&delimiter=LINE", port)
                        .process(new MockTcpProcessor())
                ;
            }
        });

        camelContext.start();
        log.info("Started MockTcpService on port {}", port);
    }

    final public void stop() throws Exception {
        camelContext.stop();
    }

    /**
     * AutoClosable.close() implementation so try-with-resources can be used
     * @throws Exception
     */
    @Override
    final public void close() throws Exception {
        stop();
    }

    /**
     * Add new expectation to current expectations
     * @param input
     * @return
     */
    final public Expectation when(final String input) {
        return new Expectation(input);
    }

    final public void loadExpectations(final String expectationsFileName) throws IOException {
        final InputStream resource = getResourceAsStream(expectationsFileName);

        expectations = new AnyExpectations()
                .loadFrom(resource);
    }

    final public void loadScript(final String scriptFileName) throws IOException {
        final InputStream resource = getResourceAsStream(scriptFileName);

        expectations = new ScriptExpectations()
                .loadFrom(resource);
    }

    final private InputStream getResourceAsStream(final String expectationsFileName) throws IOException {
        final InputStream resourceAsStream = MockTcpService.class.getClassLoader().getResourceAsStream(expectationsFileName);
        return resourceAsStream;
    }

    final public void setDefaultReply(final Reply reply) {
        this.defaultReply = reply;
    }

    /**
     * Helper class for DSL for easily adding a new expectation.
     */
    final public class Expectation {
        final private String input;

        public Expectation(String input) {
            this.input = input;
        }

        public void thenAnswer(String output) {
            expectations = expectations.add(input, output);
        }
    }

    /**
     *
     */
    interface Reply {
        public String reply(String input);
    }

    static class ReplyValue implements Reply {
        final private String value;

        public ReplyValue(final String value) {
            this.value = value;
        }

        @Override
        final public String reply(final String input) {
            return value;
        }
    }

    /**
     *
     */
    interface Expectations {
        /**
         * Get the appropriate reply for given input
         */
        Reply getReplyFor(String input);
        Expectations loadFrom(InputStream inputStream) throws IOException;
        Expectations add(String input, String answer);
    }

    /**
     * Use any expectation in the list.
     * Used expectations are not removed and can be reused multiple times.
     */
    final static class AnyExpectations implements Expectations {
        // a Map with items in order they were inserted.
        final private OrderedProperties expectations;

        private AnyExpectations(final OrderedProperties expectations) {
            this.expectations = expectations;
        }

        public AnyExpectations() {
            expectations = OrderedProperties.empty();
        }

        @Override
        final public Reply getReplyFor(final String input) {
            return createReply(findAnswer(input));
        }

        /**
         * Find answer for input.
         * @param input
         * @return Some(output) if output is found for input, otherwise None.
         */
        final private Option<String> findAnswer(final String input) {
            if(expectations.containsKey(input)) {
                return Option.of(expectations.get(input));
            }
            return Option.none();
        }

        /**
         * @param answer
         * @return A Reply for an answer.
         */
        private Reply createReply(final Option<String> answer) {
            if(answer.hasValue()) {
                return new ReplyValue(answer.get());
            } else {
                return REPLY_INPUT;
            }
        }

        @Override
        public Expectations loadFrom(InputStream inputStream) throws IOException {

            final OrderedProperties newExpectations = OrderedProperties.loadFrom(inputStream);
            return new AnyExpectations(newExpectations);
        }

        @Override
        public Expectations add(String input, String answer) {
            return new AnyExpectations(expectations.addEntry(input, answer));
        }
    }

    /**
     * Use expecations as a script, one after the other.
     * If head entry does not match with requested command, an error message the returned.
     * If head entry matches the requested command, the response is returned and command is
     * removed from the list so it expects the next command.
     */
    final static class ScriptExpectations implements Expectations {
        // a Map with items in order they were inserted.
        private OrderedProperties expectations;

        private ScriptExpectations() {
            expectations = OrderedProperties.empty();
        }

        private ScriptExpectations(OrderedProperties newExpectations) {
            expectations = newExpectations;
        }

        @Override
        public Reply getReplyFor(String input) {
            final Option<OrderedProperties.Entry> head = expectations.head();
            if(head.hasValue()) {

                final OrderedProperties.Entry entry = head.get();
                if(entry.getKey().equals(input)) {
                    expectations = expectations.tail();
                    return new ReplyValue(entry.getValue());
                }
            }
            return REPLY_INPUT_UNKNOWN;
        }

        @Override
        public Expectations loadFrom(InputStream inputStream) throws IOException {
            final OrderedProperties properties = OrderedProperties.loadFrom(inputStream);
            return new ScriptExpectations(properties);
        }

        @Override
        public Expectations add(String input, String answer) {
            return new ScriptExpectations(expectations.addEntry(input, answer));
        }
    }

    class MockTcpProcessor implements Processor {
        @Override
        public void process(Exchange exchange) throws Exception {
            String input = (String) exchange.getIn().getBody();
            log.info("Received: {}", input);
            // get reply depending on input
            Reply reply = expectations.getReplyFor(input);

            // default reply
            Object output = reply.reply(input);
            log.info("Replying: {}", output);

            exchange.getOut().setBody(output);
        }
    }
}
