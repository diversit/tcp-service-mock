package eu.diversit.tcpservice;

/**
 * A simple Option class to mimic the Option of Scala.
 * Can either be Some or None.
 * @param <T>
 */
public abstract class Option<T> {

    abstract public boolean hasValue();
    public final boolean hasNoValue() {
        return !hasValue();
    }

    abstract public T get();

    public static <T> Option<T> of(final T value) {
        if(value != null) return new Some(value);
        else return new None();
    }

    public static Option none() {
        return new None<>();
    }

    abstract public T getOrElse(final T any);

    /**
     * Represents some value.
     * @param <T>
     */
    private final static class Some<T> extends Option<T> {
        private T value;

        Some(T value) {
            this.value = value;
        }
        @Override
        public final boolean hasValue() {
            return true;
        }

        @Override
        public final T get() {
            return value;
        }

        @Override
        public final T getOrElse(final T any) {
            return get();
        }

        @Override
        public final String toString() {
            return String.format("Some(%s)", value);
        }
    }

    /**
     * Represents no value.
     * @param <T>
     */
    private final static class None<T> extends Option<T> {
        @Override
        public final boolean hasValue() {
            return false;
        }

        @Override
        public final T get() {
            throw new IllegalArgumentException("None has no value");
        }

        @Override
        public final T getOrElse(final T defaultValue) {
            return defaultValue;
        }

        @Override
        public final String toString() {
            return "None";
        }
    }
}
