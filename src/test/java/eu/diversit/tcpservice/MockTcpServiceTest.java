package eu.diversit.tcpservice;

import org.testng.annotations.Test;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class MockTcpServiceTest {

    public static final int TEST_PORT = 8888;

    @Test
    public void mockServiceShouldStartAndByDefaultEchoTheInput() throws Exception {

        withService(new BaseTestBody() {
            @Override
            public void test(MockTcpService mockTcpService) throws IOException {
                final String response = send("echo");

                assertThat(response, is("echo"));
            }
        });
    }

    @Test
    public void mockServiceShouldReturnExpectedReply() throws Exception {

        withService(new BaseTestBody() {
            @Override
            public void test(MockTcpService mockTcpService) throws IOException {

                mockTcpService.when("echo").thenAnswer("cho ho o");

                String response = send("echo");

                assertThat(response, is("cho ho o"));
            }
        });
    }

    @Test
    public void mockServiceShouldReturnResponsesFromConfiguration() throws Exception {

        withService(new BaseTestBody() {
            @Override
            public void test(MockTcpService mockTcpService) throws IOException {
                mockTcpService.loadExpectations("basic-expectations.properties");

                assertThat(send("ZM?"), is("ZM:1"));
                assertThat(send("PW?"), is("PW:1"));
                assertThat(send("ZM?"), is("ZM:1"));
            }
        });
    }

    @Test
    public void mockServiceShouldReturnResponsesFromScript() throws Exception {

        withService(new BaseTestBody() {
            @Override
            public void test(MockTcpService mockTcpService) throws IOException {
                mockTcpService.loadScript("basic-expectations.properties");

                assertThat(send("ZM?"), is("Unexpected command 'ZM?'"));
                assertThat(send("PW?"), is("PW:1"));
                assertThat(send("ZM?"), is("ZM:1"));
            }
        });
    }

    private void withService(TestBody body) throws Exception {
        try(MockTcpService mockTcpService = new MockTcpService(TEST_PORT)) {
            mockTcpService.start();

            body.test(mockTcpService);
        }
    }

    interface TestBody {
        /**
         * Implement test code here.
         * Configure mockTcpSerice as needed.
         */
        void test(MockTcpService mockTcpService) throws IOException;
    }

    /**
     * Helper implementation of {@link MockTcpServiceTest.TestBody}
     * which contains a convenience method to send and receive data to service.
     */
    abstract class BaseTestBody implements TestBody {

        /**
         * Helper method to send en receive response from mock service.
         */
        public String send(String input) throws IOException {

            try (Socket socket = new Socket("localhost", TEST_PORT)) {
                DataOutputStream das = new DataOutputStream(socket.getOutputStream());
                das.writeBytes(input + "\r\n");

                BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                String response = reader.readLine();
                return response;
            }
        }
    }
}
