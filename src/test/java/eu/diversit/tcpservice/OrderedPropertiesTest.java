package eu.diversit.tcpservice;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

public class OrderedPropertiesTest {

    private OrderedProperties orderedProperties;
    private OrderedProperties emptyProperties;

    @BeforeClass
    public void loadOrderedPropertiesFromFile() throws IOException {

        final InputStream fileAsStream = OrderedPropertiesTest.class.getClassLoader()
                .getResourceAsStream("basic-expectations.properties");

        orderedProperties = OrderedProperties.loadFrom(fileAsStream);

        emptyProperties = OrderedProperties.empty();
    }

    @Test
    public void propertiesShouldNotBeEmpty() {
        assertThat(orderedProperties.isEmpty(), is(false));
    }

    @Test
    public void propertiesSizeShouldBe2() {
        assertThat(orderedProperties.size(), is(2));
    }

    @Test
    public void propertiesShouldBeInOrderOfFileContents() {

        List<String> keys = orderedProperties.keysList();
        assertThat(keys.size(), is(2));
        assertThat(keys.get(0), is("PW?"));
        assertThat(keys.get(1), is("ZM?"));
    }

    @Test
    public void headShouldReturnFirstOption() {

        final Option<OrderedProperties.Entry> head = orderedProperties.head();
        assertThat(head.hasValue(), is(true));
        assertThat(head.get().getKey(), is("PW?"));
        assertThat(head.get().getValue(), is("PW:1"));
    }

    @Test
    public void headShouldReturnNoneIfNoELementsLeft() {


        assertThat(emptyProperties.head().hasValue(), is(false));
    }

    @Test
    public void tailShouldReturnOrderPropertiesWithoutHeadElement() {

        assertThat(orderedProperties.size(), is(2));
        final OrderedProperties tail = orderedProperties.tail();

        assertThat(tail.size(), is(1));
        assertThat(tail.head().get().getKey(), is("ZM?"));
        assertThat(tail.head().get().getValue(), is("ZM:1"));
    }

    @Test(expectedExceptions = IllegalStateException.class)
    public void tailShouldThrowExceptionWhenNoElementsLeft() {

        emptyProperties.tail();
    }

    @Test
    public void containsShouldReturnTrueWhenKeyExists() {

        assertThat(orderedProperties.containsKey("ZM?"), is(true));
    }

    @Test
    public void containsShouldReturnFalseWhenKeyDoesNotExist() {

        assertThat(orderedProperties.containsKey("non-existing"), is(false));
    }

    @Test
    public void containsShouldReturnFalseWhenPropertiesIsEmpty() {

        assertThat(emptyProperties.containsKey("test"), is(false));
    }

    @Test
    public void getShouldReturnKeyValue() {

        assertThat(orderedProperties.get("PW?"), is("PW:1"));
    }

    @Test
    public void getShouldReturnNullWhenKeyDoesNotExist() {

        assertThat(orderedProperties.get("non-existing"), is(nullValue()));
    }

    @Test
    public void getShouldReturnNullWhenPropertiesIsEmpty() {

        assertThat(emptyProperties.get("test"), is(nullValue()));
    }
}
